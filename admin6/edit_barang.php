<?php
include"koneksi.php";
$id_inventaris=$_GET['id_inventaris'];

$select=mysqli_query($koneksi,"select*from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>APLIKASI INVENTARIS </title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-angellist"></i> <span>INVENTARIS</span></a>
                    </div>
                    <div class="clearfix"></div>


                    <!-- menu prile quick info -->
                    
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>ADMIN</h3>
                            <ul class="nav side-menu">
                                <li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</span></a></li>
                                <li><a><i class="fa fa-home"></i> Inventaris <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.php">Data Barang</a>
                                        </li>
                                        <li><a href="dt_jenis.php">Data Jenis</a>
                                        </li>
                                        <li><a href="dt_ruang.php">Data Ruang</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-edit"></i> Transaksi <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="peminjaman.php">Peminjaman</a>
                                        </li>
                                        <li><a href="pengembalian.php">Pengembalian</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-desktop"></i> Petugas <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="dt_petugas.php">Data Petugas</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-table"></i>Pegawai <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="dt_pegawai.php">Data Pegawai</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-bar-chart-o"></i>Laporan <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="laporan_barang.php">Laporan Barang</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="" alt="">Logout
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="login.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>

                            <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="text-center">
                                            <a>
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Edit Data Barang</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
            <div class="form-body">
			
              <form  action="update_barang.php" method="post" enctype="multipart/form-data">
                <div class="form-group"> <label for="">Nama Barang</label> 
                  <input type="hidden" class="form-control" name="id_inventaris" required="Data belum diisi" placeholder="Nama" value="<?php echo $data['id_inventaris'];?>"> 
                  <input type="text" class="form-control" name="nama" required="Data belum diisi" placeholder="Nama" value="<?php echo $data['nama'];?>"> 
                </div>
                <div class="form-group"> <label for="">Kondisi</label> 
                  <input type="text" class="form-control" name="kondisi" required="Data belum diisi" placeholder="Kondisi" value="<?php echo $data['kondisi'];?>"> 
                </div>
                  <div class="form-group"> <label for="">Spesifikasi</label> 
                  <input type="text" class="form-control" name="spesifikasi" required="Data belum diisi" placeholder="spesifikasi" value="<?php echo $data['spesifikasi'];?>">
                </div>
                <div class="form-group"> <label for="">Keterangan</label> 
                  <input type="text"  class="form-control" name="keterangan" required="Data belum diisi" placeholder="Keterangan" value="<?php echo $data['keterangan'];?>"> 
                </div>
                <div class="form-group"> <label for="">Jumlah</label> 
                  <input type="number" class="form-control" name="jumlah" required="Data belum diisi"  placeholder="Jumlah" value="<?php echo $data['jumlah'];?>"> 
                </div>
                  <?php
                    include "koneksi.php";
                    $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                    $jsArray = "var id_jenis = new Array();\n";
                  ?>
                  <label>Jenis</label> 
                  <select class="form-control" name="id_jenis" onchange="changeValue(this.value)">
                    <option selected="selected">Pilih Jenis
                    <?php 
                    while($row = mysqli_fetch_array($result)){
                      echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                      $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                    }
                    ?>
                    </option>
                  </select>
                  <div class="form-group"> <label for="">Tanggal Register</label> 
                    <input type="date" name="tanggal_register" class="form-control" value="<?php $tanggal = Date('Y-m-d'); echo $tanggal;?>" readonly>
                  </div>
                    <?php
                      include "koneksi.php";
                      $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                      $jsArray = "var id_ruang = new Array();\n";
                    ?>
                    <label>Ruang</label> 
                    <select class="form-control" name="id_ruang" onchange="changeValue(this.value)">
                      <option selected="selected">..........Pilih Ruang..........
                      <?php 
                      while($row = mysqli_fetch_array($result)){
                        echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                        $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                      }
                      ?>
                      </option>
                    </select> 
                    <div class="form-group"> <label for="">Kode Inventaris</label> 
                      <input type="text" name="kode_inventaris" class="form-control" required="Data belum diisi" placeholder="Kode Inventaris" value="<?php echo $data['kode_inventaris'];?>"> </div>
                      <?php
                        include "koneksi.php";
                        $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
                        $jsArray = "var id_petugas = new Array();\n";
                      ?>
                      <label>ID Petugas :</label> 
                      <select class="form-control" name="id_petugas" onchange="changeValue(this.value)">
                        <option selected="selected">..........Pilih ID Petugas..........
                        <?php 
                        while($row = mysqli_fetch_array($result)){
                          echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                          $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                        }
                        ?>
                        </option>
                      </select>
                <div class="form-group"> <label for="">Sumber</label> 
                  <input type="text"  class="form-control" name="sumber" required="Data belum diisi" placeholder="sumber" value="<?php echo $data['sumber'];?>"> 
                </div> 
                      <input type="submit" value="Simpan" class="btn btn-primary">
                      <a href="index.php"  class="btn btn-success">Kembali</a>
              </form> 
            </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- footer content -->
            <footer>
                <div class="">
                    <p class="pull-right">Nava_F.
                        <span class="lead"> <i class="fa fa-paw"></i></span>
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
                
            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>
    <!-- form validation -->
    <script src="js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
    </script>

</body>
</html>